//package scanner is working with tables
package scanner

import (
	"reflect"
	"strings"
	"time"

	"gitlab.com/golight/entity/tabler"
)

type TableUpdater interface {
	SetUpdatedAt(updatedAt time.Time) tabler.Tabler
}

//Is a table itself
type Table struct {
	//A name of a table
	Name            string
	//Refers to a table fields
	Fields          []*Field
	//To get fields by their names, not by index (simpler)
	FieldsMap       map[string]*Field
	//Constraints of fields
	Constraints     []Constraint
	//Fields combined in groups
	OperationFields map[string][]*Field
	//Entity of a table, implements a tabler interface
	Entity          tabler.Tabler
}

//Returns a new TableScanner object
func NewTableScanner() Scanner {
	return &TableScanner{}
}

//Consists of tables
type TableScanner struct {
	//You can simply get tables by their names
	tables map[string]Table
}

//Adds a tabler entity into a map of entities, registrating all of its contents
func (t *TableScanner) RegisterTable(entities ...tabler.Tabler) {
	tableEntities := make(map[string]tabler.Tabler, len(entities))
	t.tables = make(map[string]Table, len(entities))
	for i := range entities {
		tableEntities[entities[i].TableName()] = entities[i]
	}

	for name, entity := range tableEntities {
		table := Table{
			Name:            name,
			FieldsMap:       make(map[string]*Field),
			OperationFields: make(map[string][]*Field),
			Entity:          entity,
		}
		reflected := reflect.TypeOf(entity).Elem()

		for i := 0; i < reflected.NumField(); i++ {
			// Get the field, returns https://golang.org/pkg/reflect/#StructField
			structField := reflected.Field(i)
			// Get the structField tag value
			fieldName := structField.Tag.Get("db")

			if fieldName == "" || fieldName == "-" {
				continue
			}

			field := &Field{
				IDx:     i,
				Name:    fieldName,
				Type:    structField.Tag.Get("db_type"),
				Default: structField.Tag.Get("db_default"),
				Table:   &table,
			}
			constraintRaw := structField.Tag.Get("db_index")
			constraintPieces := strings.Split(constraintRaw, ",")
			if len(constraintPieces) < 1 {
				field.Constraint = Constraint{}
			}
			if len(constraintPieces) > 0 {
				for i := range constraintPieces {
					switch constraintPieces[i] {
					case "index":
						field.Constraint.Index = true
					case "unique":
						field.Constraint.Unique = true
					}
				}
			}
			if field.Constraint.Index {
				field.Constraint.Field = field
				table.Constraints = append(table.Constraints, field.Constraint)
			}
			table.Fields = append(table.Fields, field)
			table.FieldsMap[field.Name] = field

			opsRaw := structField.Tag.Get("db_ops")
			ops := strings.Split(opsRaw, ",")
			if opsRaw != "" {
				for j := range ops {
					table.OperationFields[ops[j]] = append(table.OperationFields[ops[j]], field)
				}
			}

			table.OperationFields[AllFields] = append(table.OperationFields[AllFields], field)
		}

		t.tables[name] = table
	}
}

//Returns all fields' names from current table and its operation
func (t *TableScanner) OperationFieldsName(tableName string, operation string) []string {
	fields := t.tables[tableName].OperationFields[operation]
	var fieldsName []string
	for i := range fields {
		fieldsName = append(fieldsName, fields[i].Name)
	}

	return fieldsName
}

//Returns all fields' pointers from current tabler entity and its operation
func (t *TableScanner) OperationFields(table tabler.Tabler, operation string) []*Field {
	fields := t.tables[table.TableName()].OperationFields[operation]
	pointers := table.FieldsPointers()

	for i := range fields {
		fields[i].Pointer = pointers[fields[i].IDx]
	}

	return fields
}

//Returns a table by name from a map of tables
func (t *TableScanner) Table(tableName string) Table {
	return t.tables[tableName]
}

//Returns a whole map of tables
func (t *TableScanner) Tables() map[string]Table {
	return t.tables
}

//A field of a table
type Field struct {
	//An index of a field
	IDx        int
	//A name of a field
	Name       string
	//A type of a field
	Type       string
	//A default value of a field
	Default    string
	//A constraint of a field
	Constraint Constraint
	//A table this field refers to
	Table      *Table
	//A pointer to this field
	Pointer    interface{}
}

type Constraint struct {
	//Is a constraint has index or not
	Index  bool
	//Is a constraint unique or not
	Unique bool
	//Which field a constraint refers
	Field  *Field
}
