package main

import (
	"gitlab.com/golight/scanner"
)

type MockTabler struct {
	table *scanner.Table `db:"meow" db_type:"gaw" db_default:"kwa" db_index:"index,unique" db_ops:"operation1"`
}

func (m *MockTabler) TableName() string {
	return m.table.Name
}

func (m *MockTabler) OnCreate() []string {
	return []string{"field1", "field2"}
}

func (m *MockTabler) FieldsPointers() []interface{} {
	return []interface{}{nil, nil}
}

func main() {
	//Creates a new object of a table scanner
	tableScanner := scanner.NewTableScanner()
	//Creating tables...
	//Entity should be an object that implements table.Tabler interface
	mock1 := &MockTabler{}
	table1 := &scanner.Table{
		Name:        "mockTable1",
		Fields:      []*scanner.Field{},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"operation1": {&scanner.Field{Name: "field1"}},
			"operation2": {&scanner.Field{Name: "field2"}},
		},
		Entity: nil,
	}
	mock1.table = table1
	mock1.table.Entity = mock1
	//Entity should be an object that implements table.Tabler interface
	mock2 := &MockTabler{}
	table2 := &scanner.Table{
		Name:            "mockTable2",
		Fields:          []*scanner.Field{},
		FieldsMap:       map[string]*scanner.Field{},
		Constraints:     []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{},
		Entity:          nil,
	}
	mock2.table = table2
	mock2.table.Entity = mock2
	//Registering tables in a table scanner 
	tableScanner.RegisterTable(table1.Entity, table2.Entity)
}
