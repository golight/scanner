# Scanner

Interaction with tables of Golite ecosystem.

## Installation

```bash
go get "gitlab.com/golight/scanner"
```

## Available functions and methods

```go
//Adds a tabler entity into a map of entities, registrating all of its contents
func (t *TableScanner) RegisterTable(entities ...tabler.Tabler)
```

```go
//Returns all fields' names from current table and its operation
func (t *TableScanner) OperationFieldsName(tableName string, operation string) []string
```

```go
//Returns all fields' pointers from current tabler entity and its operation
func (t *TableScanner) OperationFields(table tabler.Tabler, operation string) []*Field
```

```go
//Returns a table by name from a map of tables
func (t *TableScanner) Table(tableName string) Table
```

```go
//Returns a whole map of tables
func (t *TableScanner) Tables() map[string]Table
```


## Example of implementation

```go
//Creates a new object of a table scanner
tableScanner := scanner.NewTableScanner()
//Creating tables...
//Entity should be an object that implements table.Tabler interface
table1 := &scanner.Table{
    Name:        "mockTable1",
    Fields:      []*scanner.Field{},
    FieldsMap:   map[string]*scanner.Field{},
    Constraints: []scanner.Constraint{},
    OperationFields: map[string][]*scanner.Field{
        "operation1": {&scanner.Field{Name: "field1"}},
        "operation2": {&scanner.Field{Name: "field2"}},
    },
    Entity: nil,
}
//Entity should be an object that implements table.Tabler interface
table2 := &scanner.Table{
    Name:            "mockTable2",
    Fields:          []*scanner.Field{},
    FieldsMap:       map[string]*scanner.Field{},
    Constraints:     []scanner.Constraint{},
    OperationFields: map[string][]*scanner.Field{},
    Entity:          nil,
}
//Registering tables in a table scanner 
tableScanner.RegisterTable(table1.Entity, table2.Entity)
```