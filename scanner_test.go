package scanner

import (
	"reflect"
	"testing"
)

type MockTabler struct {
	table *Table `db:"meow" db_type:"gaw" db_default:"kwa" db_index:"index,unique" db_ops:"operation1"`
}

func (m *MockTabler) TableName() string {
	return m.table.Name
}

func (m *MockTabler) OnCreate() []string {
	return []string{"field1", "field2"}
}

func (m *MockTabler) FieldsPointers() []interface{} {
	return []interface{}{nil, nil}
}

func TestNewTableScanner(t *testing.T) {
	tableScanner := NewTableScanner()
	if _, ok := tableScanner.(*TableScanner); !ok {
		t.Errorf("Expected a new table scanner object, got %T", tableScanner)
	}
}

func TestRegisterTable(t *testing.T) {
	// Create a new TableScanner
	tableScanner := &TableScanner{}

	// Create mock Tabler instances
	mockTabler1 := &MockTabler{table: &Table{
		Name:        "mockTable1",
		Fields:      []*Field{},
		FieldsMap:   map[string]*Field{},
		Constraints: []Constraint{},
		OperationFields: map[string][]*Field{
			"operation1": {&Field{Name: "field1"}},
			"operation2": {&Field{Name: "field2"}},
		},
		Entity: nil,
	}}
	mockTabler1.table.Entity = mockTabler1
	/*reflected := reflect.TypeOf(mockTabler1).Elem()
	for i := 0; i < reflected.NumField(); i++ {
		structField := reflected.Field(i)
		t.Log(structField.Tag.Get("db"))
	}*/
	mockTabler2 := &MockTabler{table: &Table{
		Name:            "mockTable2",
		Fields:          []*Field{},
		FieldsMap:       map[string]*Field{},
		Constraints:     []Constraint{},
		OperationFields: map[string][]*Field{},
		Entity:          nil,
	}}
	mockTabler2.table.Entity = mockTabler2

	// Register mock tables
	tableScanner.RegisterTable(mockTabler1, mockTabler2)
	// Check if tables are registered correctly
	if len(tableScanner.tables) != 2 {
		t.Errorf("Expected 2 tables, got %d", len(tableScanner.tables))
	}
}

func TestOperationFieldsName(t *testing.T) {
	// Create a new TableScanner
	tableScanner := &TableScanner{tables: make(map[string]Table)}

	// Mock table with operation fields
	mockTable1 := &MockTabler{table: &Table{
		Name: "mockTable1",
		OperationFields: map[string][]*Field{
			"operation1": {&Field{Name: "field1"}},
			"operation2": {&Field{Name: "field2"}},
		},
	}}

	// Mock table with operation fields
	mockTable2 := &MockTabler{table: &Table{
		Name: "mockTable2",
		OperationFields: map[string][]*Field{
			"operation3": {&Field{Name: "field3"}, &Field{Name: "field4"}},
		},
	}}

	// Register the mock tables
	tableScanner.tables["mockTable1"] = *mockTable1.table
	tableScanner.tables["mockTable2"] = *mockTable2.table

	//expected results
	expected := [][]string{{"field1"}, {"field2"}, {"field3", "field4"}}

	// Test retrieving operation fields names
	fields := tableScanner.OperationFieldsName("mockTable1", "operation1")
	if !reflect.DeepEqual(fields, expected[0]) {
		t.Errorf("Expected fields %v, got %v", expected[0], fields)
	}
	fields = tableScanner.OperationFieldsName("mockTable1", "operation2")
	if !reflect.DeepEqual(fields, expected[1]) {
		t.Errorf("Expected fields %v, got %v", expected[1], fields)
	}
	fields = tableScanner.OperationFieldsName("mockTable2", "operation3")
	if !reflect.DeepEqual(fields, expected[2]) {
		t.Errorf("Expected fields %v, got %v", expected[2], fields)
	}
}

func TestOperationFields(t *testing.T) {
	// Create a new TableScanner
	tableScanner := &TableScanner{tables: make(map[string]Table)}

	// Mock table with operation fields
	mockTable := &MockTabler{table: &Table{
		Name: "mockTable1",
		OperationFields: map[string][]*Field{
			"operation1": {&Field{Name: "field1"}},
		},
	}}

	// Register the mock table
	tableScanner.tables["mockTable1"] = *mockTable.table

	// Test retrieving operation fields
	expectedFields := []*Field{{Name: "field1"}}
	fields := tableScanner.OperationFields(mockTable, "operation1")
	if !reflect.DeepEqual(fields, expectedFields) {
		t.Errorf("Expected fields %+v, got %+v", expectedFields, fields)
	}
}

func TestTable(t *testing.T) {
	// Create a new TableScanner
	tableScanner := &TableScanner{tables: make(map[string]Table)}

	// Mock table
	mockTable := &MockTabler{table: &Table{Name: "mockTable"}}

	// Register the mock table
	tableScanner.tables["mockTable"] = *mockTable.table

	// Test retrieving table
	retrievedTable := tableScanner.Table("mockTable")

	if !reflect.DeepEqual(retrievedTable, *mockTable.table) {
		t.Errorf("Expected table %+v, got %+v", mockTable, retrievedTable)
	}
}

func TestTables(t *testing.T) {
	// Create a new TableScanner
	tableScanner := &TableScanner{tables: make(map[string]Table)}

	// Mock tables
	mockTables := map[string]MockTabler{
		"table1": {table: &Table{Name: "table1"}},
		"table2": {table: &Table{Name: "table2"}},
	}

	// Register the mock tables
	tables := make(map[string]Table, len(mockTables))
	for key, table := range mockTables {
		tables[key] = *table.table
	}
	tableScanner.tables = tables

	// Test retrieving tables
	retrievedTables := tableScanner.Tables()

	if !reflect.DeepEqual(retrievedTables, tables) {
		t.Errorf("Expected tables %+v, got %+v", tables, retrievedTables)
	}
}
