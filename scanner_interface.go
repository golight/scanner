package scanner

import "gitlab.com/golight/entity/tabler"

const (
	AllFields = "all"
	Create    = "create"
	Update    = "update"
	Upsert    = "upsert"
	Conflict  = "conflict"
)

type Scanner interface {
	RegisterTable(entities ...tabler.Tabler)
	OperationFields(table tabler.Tabler, operation string) []*Field
	OperationFieldsName(tableName string, operation string) []string
	Table(tableName string) Table
	Tables() map[string]Table
}
